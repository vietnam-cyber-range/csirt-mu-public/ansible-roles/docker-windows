# Ansible role - Docker

This role install Docker platform.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role paramaters

Optional parameters.

* `docker_windows_listen_port` - The port number that the Docker daemon will listen on (disabled by default).

    **WARNING**: This is a security risk without SSL certificate.

## Example

The simplest example.

```yml
roles:
    - role: docker-windows
```
